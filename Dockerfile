FROM php:7.2-fpm-stretch

MAINTAINER Rodrigo

RUN apt-get update && apt-get install -y libpng-dev libjpeg-dev libpq-dev libldap2-dev mysql-client zip git procps wget\
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install pdo pdo_mysql pdo_pgsql

RUN docker-php-ext-install pcntl

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/composer

RUN mkdir -p /var/www/html/

WORKDIR /var/www/html/

COPY . /var/www/html/

RUN composer install --no-dev

EXPOSE 9000

CMD ["php-fpm"]
